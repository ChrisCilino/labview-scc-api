﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="17008000">
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(]!!!*Q(C=\&gt;7R=2MR%!81N=?"5Q&lt;/07RB7W!,&lt;'&amp;&lt;9+K1,7Q,&lt;)%N&lt;!NMA3X)DW?-RJ(JQ"I\%%Z,(@`BA#==ZB3RN;]28_,V7@PWW`:R`&gt;HV*SU_WE@\N_XF[3:^^TX\+2YP)D7K6;G-RV3P)R`ZS%=_]J'XP/5N&lt;XH,7V\SEJ?]Z#5P?=J4HP+5JTTFWS%0?=B$DD1G(R/.1==!IT.+D)`B':\B'2Z@9XC':XC':XBUC?%:HO%:HO&amp;R7QT0]!T0]!S0I4&lt;*&lt;)?=:XA-(]X40-X40-VDSGC?"GC4N9(&lt;)"D2,L;4ZGG?ZH%;T&gt;-]T&gt;-]T?.S.%`T.%`T.)^&lt;NF8J4@-YZ$S'C?)JHO)JHO)R&gt;"20]220]230[;*YCK=ASI2F=)1I.Z5/Z5PR&amp;)^@54T&amp;5TT&amp;5TQO&lt;5_INJ6Z;"[(H#&gt;ZEC&gt;ZEC&gt;Z$"(*ETT*ETT*9^B)HO2*HO2*(F.&amp;]C20]C2)GN4UE1:,.[:/+5A?0^NOS?UJ^3&lt;*\9B9GT@7JISVW7*NIFC&lt;)^:$D`5Q9TWE7)M@;V&amp;D,6;M29DVR]6#R],%GC47T9_/=@&gt;Z5V&gt;V57&gt;V5E&gt;V5(OV?^T[FTP?\`?YX7ZRP6\D=LH%_8S/U_E5R_-R$I&gt;$\0@\W/VW&lt;[_"\Y[X&amp;],0^^+,]T_J&gt;`J@_B_]'_.T`$KO.@I"XC-_N!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">385908736</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Item Name="Test Suite Utilities" Type="Folder">
		<Item Name="Queue Driver" Type="Folder">
			<Item Name="Enqueue at Front.vi" Type="VI" URL="../Test Suite Utilities/Queue Driver/Enqueue at Front.vi"/>
			<Item Name="Enqueue Multiple.vi" Type="VI" URL="../Test Suite Utilities/Queue Driver/Enqueue Multiple.vi"/>
			<Item Name="Enqueue.vi" Type="VI" URL="../Test Suite Utilities/Queue Driver/Enqueue.vi"/>
		</Item>
	</Item>
	<Item Name="Tests" Type="Folder">
		<Item Name="Sync Unsync" Type="Folder">
			<Item Name="Test Dir" Type="Folder">
				<Item Name="Test file 1.txt" Type="Document" URL="/&lt;userlib&gt;/_Cirrus Logic/LabVIEW SCC API/Test Suite/Tests/Sync Unsync/Test Dir/Test file 1.txt"/>
				<Item Name="Test file 2.txt" Type="Document" URL="/&lt;userlib&gt;/_Cirrus Logic/LabVIEW SCC API/Test Suite/Tests/Sync Unsync/Test Dir/Test file 2.txt"/>
			</Item>
			<Item Name="sync unsync states.ctl" Type="VI" URL="../Tests/Sync Unsync/sync unsync states.ctl"/>
			<Item Name="Sync UnSync.vi" Type="VI" URL="../Tests/Sync Unsync/Sync UnSync.vi"/>
		</Item>
		<Item Name="Create Delete Changelists.vi" Type="VI" URL="../Tests/Create Delete Changelists.vi"/>
		<Item Name="Create Label.vi" Type="VI" URL="../Tests/Create Label.vi"/>
		<Item Name="Login Logout.vi" Type="VI" URL="../Tests/Login Logout.vi"/>
	</Item>
	<Item Name="Test Suite.vi" Type="VI" URL="../Test Suite.vi"/>
	<Item Name="Test Template.vit" Type="VI" URL="../Test Template.vit"/>
</Library>
